# Oracle candidate Appraisal Task (Backend)

This project is a backend end web Server build using Spring boot and python, the goal is to be able to send
commands via REST, run and return the results to a front end application ( e.g Jupyter like notebook)

### Getting started

```
git clone https://gitlab.com/zmazouzi/oraclecandidateappraisaltaskbackend
mvn install
mvn spring-boot:run
```

### Requirements 
Python2 or 3, openjdk 8, the maven wrapper is provided by the spring boot Initializer boilerplate  

### Dependencies
spring-boot-starter-web, Lombok, spring-tx, spring-boot-starter-test

### How it works 
The input code is parsed to check for the interpreter existence and the correctness of the syntax then creates a file in a subfolder by
the name of the interpreter the syntax should follow this schema ``%<interpreter-name><whitespace><code>`` 

### State 
For each sessionId a specific file will be created or updated if already exists the format 
of the file would be like  ``<interpreter>/<sessionId.<extension>>`` the file is then execute from the
top to bottom with the new appended code 

#### What should happen if the piece of code cannot be parsed? / interpreter is not known ?
Depending on the type of the syntax error a business exception will be thrown
e.g if the interpreter is not supported ```InterpreterUnknownException``` will be thrown


## Tests
The main functions are tested using sample input data using ```JUNIT4```

## Entry point 
```
POST : http://localhost:8080/execute
BODY : {
       	"code" : "%python print 'hello world'",
       	"sessionId": "1568751797877"
       }
```
## Sample commands

This an example of a valid input string

```
print 'hello world'
a = 1
b = 5
print a + b*2
```
the program should return by the end the 11. 

## TODO 
Flushing session if the user has not used the API for a long time 
Handling state without having to run the program from the top for each request

## License

This project is licensed under the MIT License

##
