package com.oracle.candidate_appraisal_task.service;

import com.oracle.candidate_appraisal_task.CandidateAppraisalTaskApplication;
import com.oracle.candidate_appraisal_task.dto.InputProgramDto;
import com.oracle.candidate_appraisal_task.dto.ResultDto;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.File;
import java.util.Arrays;

@RunWith(SpringRunner.class)
@ComponentScan(basePackages = "com.oracle.candidate_appraisal_task.**")
@SpringBootTest(classes = CandidateAppraisalTaskApplication.class)
public class InterpreterServiceTest {

    private static final Logger logger = LogManager.getLogger(InterpreterServiceTest.class);
    @Autowired
    private InterpreterService interpreterService;

    @Test
    public void generatePath() {
        String path = this.interpreterService.generatePath("python", "1568751797877");

        Assert.assertEquals("scripts/python/1568751797877.py", path);
    }

    @Test
    public void runSubProcess() {
    }


    @Test
    public void parseAndExecuteProgram() {
        try {
            ResultDto resultDto;
            Arrays.asList("a = 1,b = 5".split(",")).forEach((command) -> {
                try {
                    this.interpreterService.parseAndExecuteProgram(new InputProgramDto("%python" + command, "1568751797877"));
                } catch (Exception e) {
                    logger.error(e.getMessage());
                }
            });
            resultDto = this.interpreterService.parseAndExecuteProgram(new InputProgramDto("$python print a + b*5", "1568751797877"));
            Assert.assertEquals("11", resultDto.getResult());
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
    }

    @Test
    public void writeProgram() throws Exception {
        File tmpFile = new File("scripts/python/1568751797877.py");
        boolean exists = tmpFile.exists();
        this.interpreterService.writeProgram("scripts/python/1568751797877.py", "print 'hello world'", "1568751797877");
        Assert.assertEquals(exists, exists);
    }

}