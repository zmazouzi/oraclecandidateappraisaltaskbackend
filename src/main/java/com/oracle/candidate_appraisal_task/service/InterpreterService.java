package com.oracle.candidate_appraisal_task.service;

import com.oracle.candidate_appraisal_task.dto.InputProgramDto;
import com.oracle.candidate_appraisal_task.dto.ResultDto;
import com.oracle.candidate_appraisal_task.exception.CodeParsingException;
import com.oracle.candidate_appraisal_task.exception.InterpreterUnknownException;
import com.oracle.candidate_appraisal_task.exception.SubProcessRunTimeException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

import java.io.*;
import java.util.List;
import java.util.stream.Collectors;

import static com.oracle.candidate_appraisal_task.shared.CodeParameters.INTERPRETERS;

@Service
public class InterpreterService {

    private static final Logger logger = LogManager.getLogger(InterpreterService.class);


    public String generatePath(String interpreter, String sessionId) {
        return "scripts/" + interpreter + "/" + sessionId + INTERPRETERS.get(interpreter);
    }

    public void writeProgram(String filePath, String program, String sessionId) throws IOException {
        File tmpFile = new File(filePath);
        boolean exists = tmpFile.exists();
        BufferedWriter out = new BufferedWriter(new FileWriter(filePath, true));
        if (!exists) {
            out.append("print 'starting session : " + sessionId + " ...'");
        }
        out.newLine();
        out.append(program);
        out.close();
    }


    private ResultDto executeProgram(String interpreter, String program, String sessionId) throws Exception {
        String filePath = generatePath(interpreter, sessionId);
        writeProgram(filePath, program, sessionId);
        return runSubProcess(interpreter, filePath);
    }

    private ResultDto runSubProcess(String interpreter, String filePath) throws IOException {
        ProcessBuilder pb = new ProcessBuilder(interpreter, filePath);
        Process p = pb.start();
        BufferedReader in = new BufferedReader(new InputStreamReader(p.getInputStream()));
        List<String> output = in.lines().collect(Collectors.toList());
        System.out.println(p.getErrorStream());
        ResultDto resultDto = new ResultDto();
        File file = new File(filePath);
        // if the output is an empty array it means that the script crashed TODO capture of stdout of the sub process
        if (output.size() == 0) {
            file.delete();
            throw new SubProcessRunTimeException();
        }
        resultDto.setResult(output.get(output.size() - 1));
        if (resultDto.getResult() == null) {
            file.delete();
            throw new SubProcessRunTimeException();
        }
        return resultDto;
    }

    /**
     * @param inputProgramDto
     * @return saves the input program into file with corresponding sessionId and interpreter, runs and returns
     * the result
     * @throws Exception
     */
    public ResultDto parseAndExecuteProgram(InputProgramDto inputProgramDto) throws Exception {
        String[] code = inputProgramDto.getCode().split(" ", 2);
        String[] command = code[0].split("", 2);
        String interpreter = command[1];
        String program = code[1];
        String prefix = command[0];
        if (INTERPRETERS.containsKey(interpreter)) {
            try {
                if (prefix.equals("%")) {
                    return this.executeProgram(interpreter, program, inputProgramDto.getSessionId());
                } else {
                    throw new CodeParsingException();
                }
            } catch (Exception e) {
                logger.error(e.getMessage());
                throw e;
            }
        } else {
            throw new InterpreterUnknownException();
        }
    }


}

