package com.oracle.candidate_appraisal_task.exception;

public class InterpreterUnknownException extends RuntimeException {
    private String message = "Interpreter not found, please try another one";

    public InterpreterUnknownException() {
    }

    @Override
    public String getMessage() {
        return message;
    }
}
