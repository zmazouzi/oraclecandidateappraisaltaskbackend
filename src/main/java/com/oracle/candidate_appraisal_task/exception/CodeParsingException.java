package com.oracle.candidate_appraisal_task.exception;

public class CodeParsingException extends RuntimeException {
    private String message = "Could not parse program";

    public CodeParsingException() {
    }

    @Override
    public String getMessage() {
        return message;
    }
}
