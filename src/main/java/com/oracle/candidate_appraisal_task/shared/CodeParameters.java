package com.oracle.candidate_appraisal_task.shared;

import java.util.HashMap;

public class CodeParameters {
    public static HashMap<String, String> INTERPRETERS;

    static {
        INTERPRETERS = new HashMap<String, String>() {
        };
        INTERPRETERS.put("python", ".py");
    }
}
