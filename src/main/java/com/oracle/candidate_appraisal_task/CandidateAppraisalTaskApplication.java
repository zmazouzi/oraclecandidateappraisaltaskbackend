package com.oracle.candidate_appraisal_task;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CandidateAppraisalTaskApplication {

    public static void main(String[] args) {
        SpringApplication.run(CandidateAppraisalTaskApplication.class, args);
    }

}
