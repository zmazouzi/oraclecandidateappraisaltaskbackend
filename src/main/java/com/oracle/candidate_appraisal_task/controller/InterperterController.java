package com.oracle.candidate_appraisal_task.controller;

import com.oracle.candidate_appraisal_task.dto.InputProgramDto;
import com.oracle.candidate_appraisal_task.dto.ResultDto;
import com.oracle.candidate_appraisal_task.service.InterpreterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.transaction.annotation.Transactional;

import java.util.concurrent.Callable;

@CrossOrigin(origins = "http://localhost:8000")
@RestController
public class InterperterController {

    @Autowired
    InterpreterService interpreterService;

    @PostMapping(value = "/execute")
    @Transactional(timeout = 5000)
    public Callable<ResultDto> execute(@RequestBody InputProgramDto inputProgramDto) throws Exception {
        return () -> this.interpreterService.parseAndExecuteProgram(inputProgramDto);
    }

}
